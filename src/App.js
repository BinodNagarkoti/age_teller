import React, { Component } from "react";
// import logo from "./logo.svg";
import "./App.css";
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TextField from "@material-ui/core/TextField";
const currentdate = new Date();
class App extends Component {
  constructor() {
    super();
    this.state = {
      todayDate: new Date(),
      toggle: false,
      birthday: new Date(),
      birthday_Msg: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  handleChange = event => {
    console.log("newDate", event.target.value);
    this.setState({
      birthday: event.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    //if react-datepicker package is used then not need to parse date like newDate as below

    //today dates in miliseconds
    const todayDate = new Date(this.state.todayDate);
    //birthdates dates in miliseconds
    const birthday = new Date(this.state.birthday);
    // total mili seconds difference of two dates
    const totallmilliSecDiff = todayDate - birthday;
    console.log("totallmilliSecDiff", totallmilliSecDiff);
    //total seconds from miliseconds
    const totallSecDiff = Math.floor((todayDate - birthday) / 1000); //units: 1second = 1000 millisecond
    console.log("totallSecDiff:", totallSecDiff);
    //total minitues from seconds
    const totallMinDiff = Math.floor(totallSecDiff / 60); //units:1min = 60 sec
    console.log("totallMinDiff", totallMinDiff);
    //total hours from minutes
    const totallHourDiff = Math.floor(totallMinDiff / 60); //units:1 hour = 60 minutes
    console.log("totallHourDiff", totallHourDiff);
    //total day from hours
    const totallDayDiff = Math.floor(totallHourDiff / 24); //units:1 day = 24 hour
    console.log("totallDayDiff", totallDayDiff);
    //total week from days
    const totallWeekDiff = Math.floor(totallDayDiff / 7); //units:1 week = 7 day
    console.log("totallWeekDiff", totallWeekDiff);

    //total year difference of two dates
    const totallYearsDiff =
      birthday.getFullYear() === currentdate.getFullYear()
        ? 0
        : todayDate.getFullYear() - birthday.getFullYear() - 1;
    console.log("totallYearsDiff", totallYearsDiff);

    //----------------------------START Total Month Diff----------------------------------------------------------------
    //Number of days in month is not same in all months , so we need to do it differently
    // Steps :
    // 1.First we need to find number of years between two dates.
    // 2.Multiply number of years between two dates by 12(because for each year = 12 months)
    // 3.Subtract the month number(eg: June → 5 ) of second date with the month number of first date
    const totallMonthDiff =
      totallYearsDiff * 12 + (birthday.getMonth() - todayDate.getMonth()); //units:1 month !=30days
    console.log("totallMonthDiff", totallMonthDiff);
    //---------------------------------END Total Month Diff-----------------------------------------------------------

    //-----------calculating the remaining months and days---------------------------------------------------------
    //remaining months
    const remainingMonthDiff = Math.abs(
      birthday.getMonth() - todayDate.getMonth()
    );
    console.log(birthday.get);

    //remaining days.
    let dayDiff = Math.abs(birthday.getDate() - todayDate.getDate()) - 1;
    dayDiff = dayDiff === -1 ? 0 : dayDiff;

    const remainingDate =
      birthday.getDate() > todayDate.getDate() ? 30 - (dayDiff + 1) : dayDiff;
    //-----------end calculating the remaining months and days---------------------------------------------------------
    if (totallYearsDiff !== 0) {
      this.setState({
        toggle: true,
        years: totallYearsDiff,
        months: remainingMonthDiff,
        days: remainingDate,
        totallWeekDiff,
        totallDayDiff,
        totallMonthDiff
      });
    }

    if (
      totallYearsDiff !== 0 &&
      remainingMonthDiff === 0 &&
      remainingDate === 0
    ) {
      this.setState({
        birthday_Msg: "Today is You'r Birthday, Happy Birthday! Lets Celebrate"
      });
    } else {
      this.setState({ birthday_Msg: "" });
    }
  };
  render() {
    return (
      <div className="App">
        <h1> Age Teller </h1>

        <form noValidate className="form">
          <TextField
            id="date"
            label="Birthday"
            type="date"
            defaultValue={
              this.state.birthday && new Date().toISOString().slice(0, 10)
            }
            onChange={this.handleChange}
            InputLabelProps={{
              shrink: true
            }}
          />
          <button className="button" name="submit" onClick={this.onSubmit}>
            Submit
          </button>
        </form>

        {/* <DatePicker
          name="newDate"
          selected={this.state.birthday}
          onChange={this.handleChange}
          isClearable
          placeholderText="Click to select a date"
        /> */}

        {this.state.toggle ? (
          <div className="display">
            <h1>
              <span aria-label="party" role="img" style={{ fontSize: "100px" }}>
                &#127881;
              </span>
              Congratulation you'r now {this.state.years} Years{" "}
              {this.state.months} Month {this.state.days} Days from today
              <span role="img" aria-label="party" style={{ fontSize: "100px" }}>
                &#127881;
              </span>
              {this.state.birthday_Msg === "" ? (
                <div></div>
              ) : (
                <div>
                  <span
                    aria-label="party"
                    role="img"
                    style={{ fontSize: "50px" }}
                  >
                    &#127874; &#127878; &#127882; &#127873; &#127870;
                  </span>
                  <div>{this.state.birthday_Msg}</div>
                </div>
              )}
            </h1>

            <div>
              <span className="dis">Disclaimers</span>
              <p>
                The age of a person can be counted differently in different
                cultures. This calculator is based on the most common age
                system. In this system, age grows at the birthday. For example,
                the age of a person that has lived for 3 years and 11 months is
                3 and the age will turn to 4 at his/her next birthday one month
                later. Most western countries use this age system.
              </p>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    );
  }
}

export default App;
